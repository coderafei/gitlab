require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Jim < OmniAuth::Strategies::OAuth2
      option :name, 'jim'

      option :client_options, { # Defaults are set for GitLab example implementation
        site: 'http://jser.im', # The URL for your OAuth 2 server
        user_info_url: '/api/v4/users/me', # The endpoint on your OAuth 2 server that provides user info for the current user
        authorize_url: '/oauth/authorize', # The authorization endpoint for your OAuth server
        token_url: '/oauth/access_token' # The token request endpoint for your OAuth server
      }

      option :redirect_url

      uid do
        raw_info['id'].to_s
      end

      info do
        {
          'nickname' => raw_info['username'],
          'email' => raw_info['email'],
          'name' => raw_info['nickname'],
          'image' => "http://jser.im/api/v4/users/#{raw_info['id']}/image"
        }
      end

      extra do
        { raw_info: raw_info }
      end

      def raw_info
        @raw_info ||= access_token.get(options.client_options[:user_info_url]).parsed
      end

      def authorize_params
        params = super
        Hash[params.map { |k, v| [k, v.respond_to?(:call) ? v.call(request) : v] }]
      end

      def callback_url
        options.redirect_url || (full_host + script_name + callback_path)
      end
    end
  end
end

OmniAuth.config.add_camelization 'oauth2_generic', 'Jim'
